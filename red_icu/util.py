from typing import Any, Callable, Dict, Sequence

from parsimonious import VisitationError
from pyseeyou import format_tree


class LazyStr:
    __slots__ = ("translator", "keys", "kwargs")

    def __init__(
        self,
        translator: Callable[..., str],
        keys: Sequence[str],
        kwargs: Dict[str, Any],
    ):
        self.translator = translator
        self.keys = keys
        self.kwargs = kwargs

    def __deepcopy__(self, memodict=...):
        from copy import deepcopy

        return type(self)(
            translator=self.translator,
            keys=deepcopy(self.keys),
            kwargs=deepcopy(self.kwargs),
        )

    def __eq__(self, other):
        if not isinstance(other, str):
            return False
        return str(self) == other

    def __repr__(self):
        return f"<{type(self).__name__} keys={self.keys!r} kwargs={self.kwargs!r}>"

    def __str__(self):
        return self()

    # Using this for command translators requires that this discards any positional arguments passed.
    def __call__(self, *_, **kwargs):
        return self.translator(*self.keys, **self.kwargs, **kwargs)

    def __hash__(self):
        return hash(id(self))

    def format(self, **kwargs):
        return self(**kwargs)

    def __getattr__(self, item):
        return getattr(self(), item)


class ICUString(LazyStr):
    """Internal representation for a lazy string backed by ICU message formatting"""

    # noinspection PyMissingConstructor
    def __init__(self, translator: Any, locale: str, ast):
        self._translator = translator
        self._locale = locale
        self._ast = ast

    def __repr__(self):
        return f"<ICUString locale={self._locale!r} ast={self._ast!r}>"

    def __eq__(self, other):
        return (
            isinstance(other, type(self))
            # pylint:disable=protected-access
            and other._ast == self._ast
            and other._locale == self._locale
            # pylint:enable=protected-access
        )

    def __call__(self, **kwargs):
        return self.format(**kwargs)

    def __str__(self):
        return self.format()

    def format(self, **kwargs):
        for transformer in self._translator.transformers:
            transformer(self._locale, kwargs)

        try:
            return format_tree(self._ast, kwargs, self._locale)
        except VisitationError as e:
            if e.__context__ and isinstance(e.__context__, KeyError):
                raise e.__context__ from None
            raise
