import datetime
from functools import singledispatch
from inspect import getmodule

from babel.dates import format_date, format_datetime, format_time, format_timedelta
from babel.lists import format_list
from babel.numbers import format_decimal
from redbot.core.i18n import get_babel_regional_format

__all__ = ("Humanize",)


def _babel_locale(kwargs):
    kwargs["locale"] = get_babel_regional_format(kwargs.get("locale"))


@singledispatch
def humanize(val, args, kwargs):
    if callable(val):
        module = getmodule(val)
        if module and module.__name__.split(".")[0] == "babel":
            _babel_locale(kwargs)
        return val(*args, **kwargs)
    raise TypeError(f"{type(val).__name__!r} is not a recognized type nor a callable")


@humanize.register(datetime.timedelta)
def _timedelta(delta, args, kwargs):
    _babel_locale(kwargs)
    return format_timedelta(delta, *args, **kwargs)


@humanize.register(datetime.datetime)
def _datetime(dt, args, kwargs):
    _babel_locale(kwargs)
    return format_datetime(dt, *args, **kwargs)


@humanize.register(datetime.date)
def _date(date, args, kwargs):
    _babel_locale(kwargs)
    return format_date(date, *args, **kwargs)


@humanize.register(datetime.time)
def _time(time, args, kwargs):
    _babel_locale(kwargs)
    return format_time(time, *args, **kwargs)


@humanize.register(list)
@humanize.register(set)
@humanize.register(tuple)
def _sequence(seq, args, kwargs):
    _babel_locale(kwargs)
    # Sets aren't sequences, so we have to specifically cast them to lists.
    return format_list(list(seq), *args, **kwargs)


@humanize.register(int)
@humanize.register(float)
def _decimal(num, args, kwargs):
    _babel_locale(kwargs)
    return format_decimal(num, *args, **kwargs)


class Humanize:
    # noinspection PyUnresolvedReferences
    """Special placeholder class for use in translations

    Format argument values with this class type are special-cased by :class:`Translator`,
    and will instead be rendered in the output string with the formatted value for the locale
    the returned string originated from.

    This allows for more complex translation setups where you can't always be sure if you're
    loading strings from just one locale, such as with context-dependent locales.

    The following value types are supported and will be ran through their respective `Babel`_
    format functions:

    * :class:`datetime.datetime`
    * :class:`datetime.date`
    * :class:`datetime.time`
    * :class:`datetime.timedelta`
    * :class:`list`
    * :class:`set`
    * :class:`tuple`
    * :class:`int`
    * :class:`float`

    You may also specify arbitrary functions which accept both the arguments you specify
    to the Humanize class and a ``locale`` keyword argument.

    Use of any other types will result in a :class:`TypeError` being raised.

    .. _Babel: http://babel.pocoo.org/en/latest/

    Example
    --------
    >>> from red_icu import Translator, Humanize
    >>> from datetime import datetime
    >>> _ = Translator(...)
    >>> dt = datetime.now()
    >>> _("Today is {date}, and the time is currently {time}").format(date=dt.date(), time=dt.time())
    "Today is Aug 27, 2019, and the time is currently 1:33:20 PM"
    """

    __slots__ = ("value", "args", "kwargs")

    def __init__(self, value, *args, **kwargs):
        # Fail early if the given value is an unrecognized type
        if type(value) not in humanize.registry and not callable(value):
            raise TypeError(f"Unexpected type: {type(value).__qualname__!r}")

        self.value = value
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self()

    def __call__(self, *args, **kwargs):
        return humanize(self.value, self.args, {**self.kwargs, **kwargs})
