try:
    from .core import *
    from .humanize import *
except ModuleNotFoundError:
    # probably pip importing during install
    pass
